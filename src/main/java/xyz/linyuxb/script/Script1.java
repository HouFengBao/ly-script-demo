package xyz.linyuxb.script;

import xyz.linyuxb.utils.BaseScript;

import java.util.Properties;

/**
 * @Author: linyuxb
 * @Date: 2021/11/12 5:22 下午
 * @Desc:
 */
public class Script1 implements BaseScript {
    @Override
    public Boolean doScript(Properties properties) {
        System.out.println(properties.getProperty("name"));
        System.out.println("Script1 执行成功");
        return Boolean.TRUE;
    }
}
