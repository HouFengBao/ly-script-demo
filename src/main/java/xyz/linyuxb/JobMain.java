package xyz.linyuxb;

import xyz.linyuxb.utils.LyMain;

/**
 * @Author: linyuxb
 * @Date: 2021/11/12 4:47 下午
 * @Desc: JobMain 主程序启动类，默认情况下你不需要修改这里...
 */
public class JobMain extends LyMain {

    public static void main(String[] args) {
        new JobMain().run();
    }

}
