package xyz.linyuxb.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Author: linyuxb
 * @Date: 2021/11/12 6:05 下午
 * @Desc: 资源加载类
 */
public class ResourceLoad {
    private static final String FILE_NAME = "/application-%s.properties";

    public static Properties load(String eva) throws IOException {
        String format = String.format(FILE_NAME, eva);
        InputStream resourceAsStream = ResourceLoad.class.getResourceAsStream(format);
        Properties properties = new Properties();
        properties.load(resourceAsStream);
        return properties;
    }
}
