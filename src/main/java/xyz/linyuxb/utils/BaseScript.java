package xyz.linyuxb.utils;

import java.util.Properties;

/**
 * @Author: linyuxb
 * @Date: 2021/11/12 5:20 下午
 * @Desc:
 */
public interface BaseScript {
    /**
     * 执行脚本
     *
     * @param properties
     * @return
     */
    Boolean doScript(Properties properties);
}
