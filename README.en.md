# ly-script-demo 脚本服务

#### Description
**本项目旨在用java编写服务器可执行脚本，打成jar包，在服务器上执行相关逻辑**

0. 环境要求：`jdk1.8`
1. 编译命令：`mvn clean install -DskipTests`
2. 执行方法：`java -jar script_demo-1.0-SNAPSHOT.jar`
3. 参数描述：`exit 结束服务`


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
